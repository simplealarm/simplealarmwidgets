/*
Copyright 2015 Matthieu Charbonnier

This file is part of SimpleAlarm.

SimpleAlarm is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SimpleAlarm is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SimpleAlarm.  If not, see <http://www.gnu.org/licenses/>.
*/

package org.bigfoot.simplealarmwidgets;

import java.util.HashMap;

import android.app.PendingIntent;

import android.content.Context;

import android.widget.RemoteViews;

public abstract class BaseWidgetUpdater
{
  public static String INTENT_FILTER = "org.bigfoot.simplealarm.EXTERNAL_WIDGET";
  //static boolean mDefaultAdded = false;
  protected String mName = "Base Updater"; ///< human readable name of the widget
  protected Context mPkgContext = null; ///< context of the package containing the updater
  
  /** Returns a human comprehensible name to be displayed to the user
    * @return Widget name
    */
  public String getName() {return this.mName;}
  
  /** Sets the package context
   *  @param[in] context: context of the package containing the updater
   */
  public void setPackageContext(Context context){mPkgContext = context;}
  
  /** Gets the package context
   *  @return context of the package containing the updater
   */
  public Context getPackageContext(Context context){return mPkgContext;}
  
  /** Returns the widget view for the given identifier
    * @param[in] context: context in which the widget is updated
    * @param[in] appWidgetId: identifier of the widget to update
    * @return The widget views to display on the home screen
    */
  public abstract RemoteViews getViews(Context context, int appWidgetId, long now);
  
  /** @return the id that can be clicked to run SimpleAlarm*/
  public abstract int getClickableView();
  
  /** Returns the time in milliseconds at which the widget needs to be updated
    * @param[in] context: context in which the widget is updated
    * @param[in] appWidgetId: identifier of the widget to update
    * @return The time in milliseconds at which the widget needs to be updated
    */
  public abstract long getNextUpdateMillis(Context context, int appWidgetId);
}
